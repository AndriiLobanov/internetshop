﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
	public class BinaryTreeNode<T>
	{
		public BinaryTreeNode<T> _parent { get; set; } = null;
		public BinaryTreeNode<T> _leftChild { get; set; } = null;
		public BinaryTreeNode<T> _rightChild { get; set; } = null;

		public T _info { get; set; }
		public BinaryTreeNode(T item)
		{
			_info = item;
		}
		public BinaryTreeNode()
		{

		}
		public BinaryTreeNode<T> this[BinaryTreeNode<T> child]
		{
			get
			{
				return child == _leftChild ? _leftChild : _rightChild;
			}
			set
			{
				if (child != _leftChild)
				{
					_rightChild = value;
				}
				else
				{
					_leftChild = value;
				}
			}
		}
	}
}
