﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnternetStore.RealizationForInstances
{
    interface IOrders
    {
        List<Order> GetEverything();
        Order GetOrderViaID(int id);
        List<Order> GetViaUser(User user);
        bool IsExisting(int id);
    }
}
