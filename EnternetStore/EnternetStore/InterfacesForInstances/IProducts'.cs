﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnternetStore.RealizationForInstances
{
    interface IProducts_
    {
        List<Product> GetEverything();
        Product GetOrderViaID(int id);
        List<Product> GetViaUser(User user);
        bool IsExisting(int id);
    }
}
