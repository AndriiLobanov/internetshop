﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnternetStore.RealizationForInstances
{
   public interface IUsers
    {
        List<User> GetEverything();
        User GetOrderViaID(int id);
        List<User> GetViaUser(User user);
        bool IsExisting(int id);
        void Registered(User user);

    }
}
