﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace EnternetStore
{
    public enum UserRole { Guest,Users,Admin}
    public class User
    {
        public int id { get; set; }
        public string Name{ get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserRole Role{ get; set; }

    }
}
