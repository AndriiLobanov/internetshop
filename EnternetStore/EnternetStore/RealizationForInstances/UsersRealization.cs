﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EnternetStore.RealizationForInstances
{
    public class UsersRealization:IUsers
    {
        private readonly UsersCode _context;
        public UsersRealization(UsersCode context)
        {
            _context = context;
        }
        public List<User> GetEverything()
        { return _context.users; }
        public User GetOrderViaID(int id)
        {
            return _context.users.First(o => o.id == id);
        }
        public List<User> GetViaUser(User user)
        {
            return (List<User>)_context.users.Where(o => o.id == user.id);
        }
        public bool IsExisting(int id)
        {
            return _context.users.Exists(o => o.id == id);
        }

        public void Registered(User user)
        {
            _context.users.Add(user);
        }
    }
}
