﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EnternetStore.RealizationForInstances
{
    public class ProductRealization:IProducts_
    {
        private readonly UsersCode _context;
        public ProductRealization(UsersCode context)
        {
            _context = context;
        }
        public List<Product> GetEverything()
        { return _context.products; }
        public Product GetOrderViaID(int id)
        {
            return _context.products.First(o => o.Id==id);
        }
        public List<Product> GetViaUser(User user)
        {
            return (List<Product>)_context.products.Where(o => o.Id == user.id);
        }
        public bool IsExisting(int id)
        {
            return _context.products.Exists(o => o.Id == id);
        }
         
    }
}
