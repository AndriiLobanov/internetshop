﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnternetStore.RealizationForInstances
{
    public class OrderRealization:IOrders
    {
        private readonly UsersCode _context;
        public OrderRealization(UsersCode context)
        {
            _context = context;
        }
        public List<Order> GetEverything()
        { return _context.orders; }
        public Order GetOrderViaID(int id)
        {
            return _context.orders.FirstOrDefault(o => o.Id == id);
        }
        public List<Order> GetViaUser(User user)
        {
            return (List<Order>)_context.orders.Where(o => o.Id == user.id);
        }
        public bool IsExisting(int id)
        {
            return _context.orders.Exists(o => o.Id == id);
        }
    }
}
