﻿namespace EnternetStore
{
    public class Product
    {
        public int Id { get; set; }
        public string DescriptionOfProduct { get; set; }
        public string NameOfProduct{ get; set; }
        public double Price{ get; set; }
           public string Type { get; set; }
    }
}
