﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnternetStore
{
    public class Order
    {
        public int Id { get; set; }
        public int UserId { get; set; }

        public List<string> NamesOfProduct { get; set; }
        public List<int> IdsOfProduct { get; set; }

        
    }
}
