﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnternetStore
{
    public class UsersCode
    {
        public List<User> users;
        public List<Product> products;
        public List<Order> orders;

        public UsersCode()
        {
            users = new List<User>();
            products = new List<Product>();
            orders = new List<Order>();
            ExecuteCode();
        }
        public void ExecuteCode()
        {
            var p1 = new Product() { Type = "Sedan", NameOfProduct = "Mazda 6", Price = 500000, DescriptionOfProduct = "Klassnoe Avto" , Id=0 };
            var p2 = new Product() { Type = "Crossover", NameOfProduct = "Reno Duster", Price = 40000, DescriptionOfProduct = "Ne ochen",Id=1 };
            var p3 = new Product() { Type = "Jeep", NameOfProduct = "Toyota Land Cruiser", Price = 100000000, DescriptionOfProduct = "Ne slomayetsya nikogda", Id=2 };
            products.Add(p1);
            products.Add(p2);
            products.Add(p3);

            var u1 = new User() { id = 0, Name = "Andriy", LastName = "Lobanov", Email = "mister.loban2013@gmail", Role = UserRole.Users };
            var u2 = new User() { id = 1, Name = "Semen", LastName = "Semenchenko", Email = "semensemecnchenko@gmail", Role = UserRole.Users };
            var u3= new User() { id = 2, Name = "Petr", LastName = "Doroshenko", Email = "doroh003@gmail", Role = UserRole.Users };
            users.Add(u1);
            users.Add(u2);
            users.Add(u3);
            var o1 = new Order() { Id = 0, UserId = 0, IdsOfProduct = new List<int> { p1.Id }, NamesOfProduct = new List<string> { p1.NameOfProduct } };
            var o2 = new Order() { Id = 1, UserId = 1, IdsOfProduct = new List<int> { p2.Id }, NamesOfProduct = new List<string> { p2.NameOfProduct } };

            orders.Add(o1);
            orders.Add(o2);

        }
    }
}
