using System;
using Xunit;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using EnternetStore;
using EnternetStore.RealizationForInstances;
namespace ServicesTests
{
    public class UnitTest1
    {
        public UsersCode _context;
        [Fact]
        public void GetUsers_ThenReturnCorrectList()
        {
            List<User> u1= new List<User>();
            u1.Add(new User() { id = 0 });
            u1.Add(new User() { id = 1 });
            u1.Add(new User() { id = 2 });

            var users = new UsersRealization(_context);
            var actual = users.GetEverything();

            Assert.Equal(3, actual.Count);
            Assert.True(actual.ElementAt(0).id == 0);
            Assert.True(actual.ElementAt(1).id == 1);

        }
        [Fact]
        public void GetOrders_ThenReturnCorrectList()
        {
            List<Order> u1 = new List<Order>();
            u1.Add(new Order() { Id = 0 });
            u1.Add(new Order() { Id = 1 });
            u1.Add(new Order() { Id = 2 });

            var orders = new ProductRealization(_context);
            var actual = orders.GetEverything();

            Assert.Equal(3, actual.Count);
            Assert.True(actual.ElementAt(0).Id == 0);
            Assert.True(actual.ElementAt(1).Id == 1);
        }
        [Fact]
        public void GetProducts_ThenReturnCorrectList()
        {
            List<Product> u1 = new List<Product>();
            u1.Add(new Product() { Id = 0 });
            u1.Add(new Product() { Id = 1 });
            u1.Add(new Product() { Id = 2 });

            var products = new ProductRealization(_context);
            var actual = products.GetEverything();

            Assert.Equal(3, actual.Count);
            Assert.True(actual.ElementAt(0).Id == 0);
            Assert.True(actual.ElementAt(1).Id == 1);
        }
        [Fact]
        public void UpdateUserInfo()
        {
            var user = new User() { id = 2 };
            List<User> u1 = new List<User>();
            u1.Add(new User() { id = 0 });
            u1.Add(new User() { id = 1});

            var usersActual = new UsersRealization(_context);
             usersActual.Registered(user);
           var actual= usersActual.IsExisting(2);

            Assert.True(actual);
        }
        [Fact]
        public void GetUsersOrders_ReturnCorrectOrderes()
        {
            var user = new User() { id = 0 };
            List<Product> u1 = new List<Product>();
            u1.Add(new Product() { Id = 1 });
            u1.Add(new Product() { Id = 2 });
            List<Order> o1 = new List<Order>();
            o1.Add(new Order() { Id = 0,UserId=user.id});
            o1.Add(new Order() { Id = 1, UserId = user.id });

            var orders = new ProductRealization(_context);
            var actual = orders.GetEverything();

            Assert.Equal(2, actual.Count);

        }
    }
}
