﻿using System;
using System.Linq;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static void ChangeElementsInArray(int[] nums)
        {
            if (nums.Length % 2 == 0)
            {
                for (var i = 0; i < nums.Length / 2; ++i)
                {
                    if (nums[i] % 2 == 0 && nums[nums.Length - 1 - i] % 2 == 0)
                    {
                        var res = nums[i];
                        nums[i] = nums[nums.Length - i - 1];
                        nums[nums.Length - i - 1] = res;
                    }
                }
            }
            else
            {
                for (var i = 0; i < (nums.Length / 2)+1; ++i)
                {
                    if (nums[i] % 2 == 0 && nums[nums.Length - 1 - i] % 2 == 0)
                    {
                        var res = nums[i];
                        nums[i] = nums[nums.Length - i - 1];
                        nums[nums.Length - i - 1] = res;
                    }
                }
            }

        }
        /// <summary>
        /// Task 2
        /// </summary>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            if (nums.Length == 0 || nums.Length == 1)
                return 0;
            int max = nums.Max();
            int first = Array.IndexOf(nums, max);
            int last = Array.LastIndexOf(nums, max);
            int distance = Math.Abs(last - first);
            if (first== last) return 0;
            return distance; 
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); ++i)
            {
                for (int j = 0; j < matrix.GetLength(1); ++j)
                {
                    if (i>j)//под диагональю
                        matrix[i, j] = 0;
                    if (j>i)//над диагональю
                        matrix[i, j] = 1;
                }
            }
        }
    }
}
