using System;
using System.Security.Cryptography.X509Certificates;

namespace Condition
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
        {
            if (n > 0)
                return n*n;
           else if (n < 0) 
                return Math.Abs(n);
            else return 0;

        }

        /// <summary>
        /// Implement code according to description of  task 2
        /// </summary>  
        public static int Task2(int n)
        {
            int m= n ;
            int k = 0;
            int n1 = 0;
            for (int i = 9; i >=0; --i)
            {
                m = n;
                while (m > 0)
                {
                    k = m % 10;
                    if (k == i)
                        n1 = 10 * n1 + k;
                    m /= 10;
                }
            }
            return n1;        
        }
    }
}
