﻿using System;
using System.Collections;
using System.Threading;

namespace LoopTasks
{
    public static class LoopTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static int SumOfOddDigits(int n)
        {
            int sum = 0;
            int m = n;
            while (m > 0)
            {
                int k = m % 10;
                if (k % 2 != 0)
                    sum += k;
                m /= 10;
            }
            return sum;
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            int[] num = new int[1];
            num[0] = n;
            BitArray ba = new BitArray(num);
            int counter = 0;
            for (int i = 0; i < ba.Count; ++i)
                if (ba[i]) counter++;
            return counter;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int a = 0;
            int b = 1;
            int tmp;
            for (int i = 0; i < n; i++)
            {
                {
                    tmp = a;
                    a = b;
                    b += tmp;
                }
            }
            return b-1;
        }
    }
}